  @posicionX = 0
  @posicionY = 0
  @sentido = ''

  def iniciar(coordenadas)
    @posicionX = coordenadas[0].to_i
    @posicionY = coordenadas[1].to_i
    @sentido = coordenadas[2].to_s
  end

  def run(ordenes)
    ordenes.each_char do |i|
      case i
        when 'L'
          girar('L')
        when 'R'
          girar('R')
        when 'M'
          mover(@sentido)
      end
    end
  end

  def mover(direccion)
    case direccion
      when 'N'
        @posicionY = @posicionY + 1
      when 'S'
        @posicionY = @posicionY - 1
      when 'E'
        @posicionX = @posicionX + 1
      when 'W'
        @posicionX = @posicionX - 1
    end
  end

  def girar(lado)
    case @sentido
      when 'N'
        norte(lado)
      when 'S'
        sur(lado)
      when 'E'
        este(lado)
      when 'W'
        oeste(lado)
    end
  end

  def norte(lado)
    case lado
      when 'L'
        @sentido = 'W'
      when 'R'
        @sentido = 'E'
    end
  end

  def sur(lado)
    case lado
      when 'L'
        @sentido = 'E'
      when 'R'
        @sentido = 'W'
    end
  end

  def este(lado)
    case lado
      when 'L'
        @sentido = 'N'
      when 'R'
        @sentido = 'S'
    end
  end

  def oeste(lado)
    case lado
      when 'L'
        @sentido = 'S'
      when 'R'
        @sentido = 'N'
    end
  end

  iniciar('12N')
  run('LMLMLMLMM')
  puts '=========='
  print "#{@posicionX} #{@posicionY} #{@sentido}"